package Simulation;

import Path.PathFind_A;

public class Simulation {
	
	private float time;
	private int distance;
	
	public Simulation() {
		this.time = 0;
		this.distance = 0;
	}
	
	public Simulation(PathFind_A path_param, int distance_param){
		this.time = 0;
		this.distance = distance_param;
	}
	
	//Getters
	public float getTime(){
		this.time =   (float) ( 0.5 * (float) this.distance); 
		return this.time;
	}
	
	public int getDistance(){
		return this.distance;
	}
	
	//Setters
	public void setDistance(int distance_param){
		this.distance = distance_param;
	}
	
	public void incrementDistance(){
		this.distance++;
	}
}
