package Database;

import java.util.*;
import java.sql.*;
import Path.Point;

public class Sql { 


	// connexion à la bdd
	public static Statement connectBDD() throws SQLException{
		Statement stmt;
		Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/testJava?autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "eisti0001");
		stmt = conn.createStatement();	
		return stmt;
	}


	//quand on lance le programme pour xla premiere fois, initialisation des tables
	public static void firstTime() {
		String init1,init2,init3;
		init1 = "CREATE TABLE IF NOT EXISTS Plan ("
				+ "nomP varchar(32) UNIQUE,"
				+ "width int(16),"
				+ "height int(16),"
				+ "idP int(8) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT"
				+ ");";

		init2 = "CREATE TABLE IF NOT EXISTS ObstacleDuPlan ("
				+ "idPlan int(8) NOT NULL, "
				+ "posX int(16) NOT NULL,"
				+ "posY int(16) NOT NULL,"
				+ "FOREIGN KEY (idPlan) REFERENCES Plan(idP)"
				+ ");";

		init3 = "CREATE TABLE IF NOT EXISTS Parcours ("
				+ "idParcours int(8) PRIMARY KEY NOT NULL UNIQUE AUTO_INCREMENT,"
				+ "idPlan int(8),"
				+ "tempsP float(16),"
				+ "distanceP int(16),"
				+ "pDepartX int(16),"
				+ "pDepartY int(16),"
				+ "pArriveX int(16),"
				+ "pArriveY int(16),"
				+ "methode varchar(32),"
				+ "FOREIGN KEY (idPlan) REFERENCES Plan(idP)"
				+ ");"; 


		try (Statement stmt = connectBDD();) {
			requeteUpdate(init1,stmt);
			requeteUpdate(init2,stmt);
			requeteUpdate(init3,stmt);
		} catch (Exception e) {
		}
	}	



	public static ArrayList<Float> getPerf(String nomPlan, Point pDepart, Point pArrive, String methode){
		int idParcours = 0;
		ArrayList<Float> res = new ArrayList<Float>();
		try (Statement stmt = connectBDD();) {
			idParcours = requeteQuery("SELECT idParcours FROM Parcours WHERE idPlan =(SELECT idPlan FROM Plan WHERE nomP = '" + nomPlan + "') AND pDepartX = " + pDepart.getX() + " AND pDepartY = " + pDepart.getY() + " AND pArriveX = " + pArrive.getX() + " AND pArriveY = " + pArrive.getY() + " AND methode = '" + methode + "';",stmt);
			Float tempsP = requeteFloat("SELECT tempsP FROM Parcours WHERE idParcours = " + idParcours + ";",stmt);
			res.add(tempsP);
			Float distanceP = requeteFloat("SELECT distanceP FROM Parcours WHERE idParcours = " + idParcours + ";",stmt);
			res.add(distanceP);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return res;
	}


	//renvoie toutes les infos sur les performances
	public static ArrayList<ArrayList<String>> infoPerf(String nomPlan){
		int i = 0;
		ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();
		ArrayList<String> tmp = new ArrayList<String>();
		try (Statement stmt = connectBDD();) {
			tmp = requetePerf("SELECT DISTINCT idPlan, pDepartX, pDepartY, pArriveX, pArriveY FROM Parcours WHERE idPlan = (SELECT idP FROM Plan WHERE nomP= '"+nomPlan+"') LIMIT " + i + ",1 ;",stmt);
			while (tmp.get(0) != "") {
				res.add(tmp);
				tmp.set(0, requeteString("SELECT nomP FROM Plan WHERE idP = "+ tmp.get(0) + ";",stmt)); 
				i++;
				tmp = requetePerf("SELECT DISTINCT idPlan, pDepartX, pDepartY, pArriveX, pArriveY FROM Parcours WHERE idPlan = (SELECT idP FROM Plan WHERE nomP= '"+nomPlan+"') LIMIT " + i + ",1;",stmt);
			} 
		} catch (Exception e) {
			// TODO: handle exception
		}
		return res;
	}

	//renvoie toutes les infos sur les performances
	public static ArrayList<ArrayList<String>> comparePerf(String nomPlan, Point pDepart, Point pArrive){
		ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();
		ArrayList<String> tmp = new ArrayList<String>();
		int i = 0;
		try (Statement stmt = connectBDD();) {
			while (i != 3){
				tmp = requetePerf("SELECT tempsP,distanceP, methode FROM Parcours WHERE idPlan = (SELECT idP FROM Plan WHERE nomP = '"+ nomPlan + "') AND pDepartX = " + pDepart.getX() + " AND pDepartY = " + pDepart.getY() + " AND pArriveX = " + pArrive.getX() +" AND pArriveY = " + pArrive.getY() + " LIMIT " + i +",1;",stmt);
				if (tmp.isEmpty()){
					tmp.add("0");
					tmp.add("0");
					tmp.add("0");
				} 
				res.add(tmp);
				i++;
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return res;
	}


	//ajout des performances de la simulation pour un plan donné
	public static void ajoutParcours(String nomPlan,int distanceP, float tempsP, Point pDepart, Point pArrive, String methode){
		String ajoutPlan;
		boolean dejaParcouru;
		try (Statement stmt = connectBDD();) {
			dejaParcouru = requeteBool("SELECT * FROM Parcours WHERE idPlan = (SELECT idP FROM Plan WHERE nomP = '" + nomPlan + "') AND pDepartX = " + pDepart.getX() + " AND pDepartY = " + pDepart.getY() + " AND pArriveX =" + pArrive.getX() + " AND pArriveY = " + pArrive.getY() + " AND methode = '" + methode + "';",stmt);
			if (!dejaParcouru){
				ajoutPlan = "INSERT INTO Parcours(idPlan,tempsP,distanceP,pDepartX,pDepartY,pArriveX,pArriveY,methode) VALUES ((SELECT idP FROM Plan WHERE nomP = '" + nomPlan + "')," + tempsP + "," + distanceP + ","+ pDepart.getX() +"," + pDepart.getY() + "," + pArrive.getX() + "," + pArrive.getY()+ ",'" + methode + "'" + ");";
				requeteUpdate(ajoutPlan,stmt);
			} else {
				System.out.println("Chemin deja enregistré!");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//ajout de la carte dans la bdd
	public static void ajoutCarte(int height, int width, String nomPlan, List<Point> listObstacles) {
		String ajoutPlan;
		int idPlan = 0;
		boolean nomdejaPris;
		try (Statement stmt = connectBDD();) {
			nomdejaPris = requeteBool("SELECT * FROM Plan WHERE (nomP = '"+ nomPlan+ "' AND height = "+ height + " AND width =" + width + ");",stmt);
			if (!nomdejaPris){
				ajoutPlan = "INSERT INTO Plan(nomP, height, width) VALUES ('"  + nomPlan + "'," + height + "," + width +");";
				requeteUpdate(ajoutPlan,stmt);
				idPlan = requeteQuery("SELECT idP FROM Plan WHERE nomP = '" + nomPlan + "';",stmt);
				while (!listObstacles.isEmpty()){
					requeteUpdate("INSERT INTO ObstacleDuPlan VALUES ("  + idPlan + "," + listObstacles.get(0).getX() + "," +
							listObstacles.get(0).getY() + ");",stmt); 
					listObstacles.remove(0);
				}
			} else {
				System.out.println("Nom déjà utilisé!");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//modifie le plan venant de la bdd
	public int[][] modifPlan(String nomPlan){
		int[][] res = new int[chargePlan(nomPlan).length][chargePlan(nomPlan)[0].length];
		res = chargePlan(nomPlan);
		supprimePlan(nomPlan);
		return res;
	}

	//supprime le plan de la bdd
	public static void supprimePlan(String nomPlan){
		int idPlan;
		try (Statement stmt = connectBDD();) {
			idPlan = requeteQuery("SELECT idP FROM Plan WHERE nomP ='" + nomPlan + "';",stmt);
			requeteUpdate("DELETE FROM ObstacleDuPlan WHERE idPlan = " + idPlan,stmt);
			requeteUpdate("DELETE FROM Plan WHERE idP = " + idPlan,stmt);
			requeteUpdate("DELETE FROM Parcours WHERE idPlan = " + idPlan,stmt);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// charge le plan de la bdd sous forme de matrice
	public static int[][] chargePlan(String nomPlan) {
		ArrayList<Point> listObstacles; 
		int idPlan;
		int height = 0;
		int width = 0;
		int[][] matEntier;
		matEntier = new int [0][0];
		try (Statement stmt = connectBDD();) {
			idPlan = requeteQuery("SELECT idP FROM Plan WHERE nomP ='" + nomPlan + "';",stmt);
			height = requeteQuery("SELECT height FROM Plan WHERE nomP ='" + nomPlan + "';",stmt);
			width = requeteQuery("SELECT width FROM Plan WHERE nomP ='" + nomPlan + "';",stmt);
			matEntier = new int[height][width];
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					matEntier[i][j] = 0;
				}
			}
			listObstacles = requeteObstacle("SELECT posX,posY FROM ObstacleDuPlan WHERE idPlan ='" + idPlan + "';",stmt);
			while (!listObstacles.isEmpty()){
				matEntier[listObstacles.get(0).getX()][listObstacles.get(0).getY()] = 1;
				listObstacles.remove(0);
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return matEntier;
	}	

	//renvoie les infos sur tous les plans de la bdd
	public static ArrayList<ArrayList<String>> infoPlan() {
		int idPlan;
		int i = 0;
		String nomPlan;
		ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();
		try (Statement stmt = connectBDD();) {
			nomPlan = requeteString("SELECT nomP FROM Plan ORDER BY `idP` ASC LIMIT "+ i + ",1;",stmt);
			while (nomPlan != ""){
				ArrayList<String> inner = new ArrayList<String>();
				inner.clear();
				inner.add(nomPlan);
				inner.add(String.valueOf(requeteQuery("SELECT height FROM Plan WHERE nomP ='" + nomPlan + "';",stmt)));
				inner.add(String.valueOf(requeteQuery("SELECT width FROM Plan WHERE nomP ='" + nomPlan + "';",stmt)));
				idPlan = requeteQuery("SELECT idP FROM Plan LIMIT "+ i + ",1;",stmt);
				inner.add(String.valueOf(requeteQuery("SELECT COUNT(posX) FROM ObstacleDuPlan WHERE idPlan ='" + idPlan + "';",stmt)));
				res.add(inner);
				i++;
				nomPlan = requeteString("SELECT nomP FROM Plan ORDER BY `idP` ASC LIMIT "+ i + ",1;",stmt);

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return res;
	}	

	//renvoie les positions des obstacles de la requete
	public static ArrayList<Point> requeteObstacle(String requeteaFaire,Statement stmt ) {	
		ResultSet rset = null;
		ArrayList<Point> res = new ArrayList<Point>();
		try {
			rset = stmt.executeQuery(requeteaFaire);
			while (rset.next()) {
				Point pointA = new Point();
				pointA.setX(rset.getInt(1));
				pointA.setY(rset.getInt(2));
				res.add(pointA);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	//retourne vrai si la bdd contient déjà l'objet à ajouter
	public static boolean requeteBool (String requeteaFaire, Statement stmt) {	
		ResultSet rset = null;
		boolean res = false;
		try {
			rset = stmt.executeQuery(requeteaFaire);
			while (rset.next()) {
				res = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (res);
	}

	//retourne un entier selon la requete sql
	public static int requeteQuery(String requeteaFaire, Statement stmt) {	
		ResultSet rset = null;
		int res = 0;
		try {
			rset = stmt.executeQuery(requeteaFaire);
			while (rset.next()) {
				res = rset.getInt(1);
			}
		} catch(SQLException ex) {
			ex.printStackTrace();
		}
		return res;
	}

	//retourne un float selon la requete sql
	public static Float requeteFloat(String requeteaFaire, Statement stmt) {	
		ResultSet rset = null;
		Float res = 0f;
		try {
			rset = stmt.executeQuery(requeteaFaire);
			while (rset.next()) {
				res = rset.getFloat(1);
			}
		} catch(SQLException ex) {
			ex.printStackTrace();
		}
		return res;
	}

	//retourne une chaine de caractères selon la requete sql
	public static String requeteString(String requeteaFaire, Statement stmt) {	
		ResultSet rset = null;
		String res = "";
		try {
			rset = stmt.executeQuery(requeteaFaire);
			while (rset.next()) {
				res = rset.getString(1);
			}
		} catch(SQLException ex) {
			ex.printStackTrace();
		}
		return res;
	}

	//retourne plusieurs chaine de caractères selon la requete sql
	public static ArrayList<String> requetePerf(String requeteaFaire, Statement stmt) {	
		ResultSet rset = null;
		int i = 1;
		ArrayList<String> res = new ArrayList<String>();
		try {
			rset = stmt.executeQuery(requeteaFaire);
			while (rset.next()) {
				while (rset.getMetaData().getColumnCount() != (i-1)){
					res.add(rset.getString(i));
					i++;
				}
			}
		} catch(SQLException ex) {
			ex.printStackTrace();
		}
		return res;
	}

	//requete de type update
	public static void requeteUpdate(String requeteaFaire, Statement stmt) {	
		try {
			stmt.executeUpdate(requeteaFaire);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

