package Path;

import java.util.ArrayList;
import java.util.Stack;


public abstract class PathFind {
	
	protected Icase [][]iCCarte;
	public Stack<Point> chemin= new Stack<Point>();
	
	public PathFind () {
	}
	
	/**
	 * @author Aleonard vincent
	 * @version 1.0
	 * @param x longueur du tableau
	 * @param y largeur du tableau
	 * Constructeur de la classe pour un fonctionnement standard à la taille X Y
	 */
	public PathFind (int x, int y, int[][] plan) {

		//Déclaration des variables de boucle I et J.
		int i,j;

		//On définit que la matrice de classe sera du type Icase
		this.iCCarte=new Icase[x][y];

		//Pour chaque i allant à la taille de X
		for (i=0;i<x;i++){

			//Pour chaque J allant à la taille de Y
			for (j=0;j<y;j++){
				//On déclare chaque case on va définir qu'elle est du type Icase
				this.iCCarte[i][j] = new Icase();
				if(plan[i][j]!=0){
					this.iCCarte[i][j].setType(plan[i][j]);
				}
			}
		}

	}
	
	public void m_PathFind_A(Point departPoint, Point arrivalPoint) {		
	}
}
