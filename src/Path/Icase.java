package Path;

public class Icase {
	
	private int type;
	private  int cost_total;
	private int cost_to_D;
	private int cost_to_A;

	public Icase(){
		
		type=0;
		cost_total=0;
		cost_to_D=0;
		cost_to_A=0;
	
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getCost_total() {
		return cost_total;
	}

	public void setCost_total(int cost_total) {
		this.cost_total = cost_total;
	}

	public int getCost_to_D() {
		return cost_to_D;
	}

	public void setCost_to_D(int cost_to_D) {
		this.cost_to_D = cost_to_D;
	}

	public int getCost_to_A() {
		return cost_to_A;
	}

	public void setCost_to_A(int cost_to_A) {
		this.cost_to_A = cost_to_A;
	}
	
	
	
	
}

