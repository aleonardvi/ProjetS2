package Path;

public class Point {
	private int x;
	private int y;
	
	
	
	public Point() {
		x = 0;
		y = 0;
	}	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * 
	 * @param p
	 * @return
	 */
	public int  distanceDA(Point p){
		//On retourne la valeur calculée par la méthode.
		return Math.abs(x-p.getX())+ Math.abs(y - p.getY());
	}

	public Point getPointHaut(){
		//On retourne un nouveau point qui représente le point haut du point initiale
		return new Point(x-1,y);
	}

	public Point getPointBas(){
		//On retourne un nouveau point qui représente le point bas du point initiale
		return new Point(x+1,y);
	}
	public Point getPointGauche(){
		//On retourne un nouveau point qui représente le point gauche du point initiale
		return new Point(x,y-1);
	}

	public Point getPointDroite(){
		//On retourne un nouveau point qui représente le point bas du point initiale
		return new Point(x,y+1);
	}
	
	public Point getPointHautGauche(){
		//On retourne un nouveau point qui représente le point bas du point initiale
		return new Point(x-1,y-1);
	}
	
	public Point getPointHautDroite(){
		//On retourne un nouveau point qui représente le point bas du point initiale
		return new Point(x-1,y+1);
	}
	
	public Point getPointBasGauche(){
		//On retourne un nouveau point qui représente le point bas du point initiale
		return new Point(x+1,y-1);
	}
	
	public Point getPointBasDroite(){
		//On retourne un nouveau point qui représente le point bas du point initiale
		return new Point(x+1,y+1);
	}
	
	
	
	
		

}
