/*
 * Author : Aleonard Vincent
 * Date : 12/04/2018
 * Vers 1.0
 * Description : Class contenant les méthodes et autre informations permettant le calcul de trajectoire.  
 * */


package Path;


import java.util.*;


public class PathFind_A extends PathFind {
	
	public  PathFind_A (int x, int y, int[][] plan) {
		super(x,y,plan);
	}

	/*fonction qui va permettre de définir :
	 * int miCarte [][] : la carte/matrice ou l'on se trouve
	 * int viDepart[] 	: Position de départ (tableau de X|Y).
	 * int viArrive[] 	: Position d'arrivé (tableau de X|Y).
	 */
	/**
	 * 
	 * @param viDepart 
	 * @param viArrive 
	 */
	@Override
	public void m_PathFind_A(Point pDepart, Point pArrive){

		ArrayList <Point> list = new ArrayList<Point>();  
		boolean bArrive=false;
		Point pPoint=new Point();
		

		//On ajoute la case de départ.
		list.add(pDepart);

		//Tant que la liste n'est pas vide ou que l'on est pas arrivé on continue
		while (!(list.isEmpty() || bArrive)){
			
			//On récupére la première valeur X de la liste
			pPoint.setX(list.get(0).getX());
	
			//On récupére la première valeur Y de la liste
			pPoint.setY(list.get(0).getY());
			
			//On enléve de la liste la première valeur de la liste.
			list.remove(0);
			
			//Faire en sorte qu'il n'y ait qu'un appel de la fonction
			/*On fait pour chacune des cases adjacentes le calcul de la distance au point d'arrivé */
			calculDistance(pPoint.getPointBas()		,list,pArrive,iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D());
			calculDistance(pPoint.getPointHaut()	,list,pArrive,iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D());
			calculDistance(pPoint.getPointGauche()	,list,pArrive,iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D());
			calculDistance(pPoint.getPointDroite()	,list,pArrive,iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D());


			//Si la valeur de la case a été modifié par l'application
			if (iCCarte[pArrive.getX()][pArrive.getY()].getCost_total()!=0){

				/*Partie Visuel pour le débug**/
				iCCarte[pArrive.getX()][pArrive.getY()].setCost_total(-1);
				/*Fin de la partie visuel pour le débug*/

				//On informe le programme que l'on est arrivé
				bArrive=true;
				setChemin(pDepart, pArrive);


			}
		}
	}
	
	public void calculDistance(Point pPoint, ArrayList<Point> list,Point pArrive,int iPrecedent)
	{
		//Valeur booléan qui permet de d'arreter le triage après réussite du trie
		boolean bPlacer=true;

		//Déclaration de la valeur i qui sert de valeur index dans la liste
		int i=0;

		//Si le point est en dehors du tableau on ne continue pas la fonction.
		if (!((pPoint.getX()<0) || (pPoint.getY()<0) || (pPoint.getY() >= iCCarte[0].length) || (pPoint.getX() >= iCCarte.length))) {

			//Tester si la case est dispo

			if (((iCCarte[pPoint.getX()][pPoint.getY()].getType()==0)||(iCCarte[pPoint.getX()][pPoint.getY()].getType()==2)||(iCCarte[pPoint.getX()][pPoint.getY()].getType()==3)) && (iCCarte[pPoint.getX()][pPoint.getY()].getCost_total()==0)){

				//Définir la distance entre le point et l'arrivé
				iCCarte[pPoint.getX()][pPoint.getY()].setCost_to_A(pPoint.distanceDA(pArrive) );
				iCCarte[pPoint.getX()][pPoint.getY()].setCost_to_D(iPrecedent+1);

				// Ajouter la distance trouver avec le nombre d'étape précédent
				iCCarte[pPoint.getX()][pPoint.getY()].setCost_total(iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_A()+iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D());

				/* **** Ajout dans la liste pour la prochaine utilisation***/

				//Si la liste est vide
				if (list.isEmpty()){
					//On ajoute à la liste la case
					list.add(pPoint);

				}else{

					//Tant que l'on n'a pas
					while (bPlacer){

						//Si la valeur est inférieur à la nouvelle valeur on passe à la valeur suivante dans la liste
						if (iCCarte[list.get(i).getX()][list.get(i).getY()].getCost_total()<iCCarte[pPoint.getX()][pPoint.getY()].getCost_total()){

							//On passe à la valeur suivante
							i++;


						}else{

							//Si la valeur est plus petite que la prochaine valeur dans la liste
							//On ajoute à la liste la case
							list.add(i,pPoint);

							//La case est placée dans la liste
							bPlacer=false;

						}

						//Si i est arrivé à la fin de la liste sans avoir trouvé de valeur plus grande que celle de itab_buff
						if (i>=(list.size()-1)){

							//On ajoute à la fin de la liste itab_buff 
							list.add(i,pPoint);

							//La case est placée dans la liste
							bPlacer=false;

						}
					}
				}
			}
		}

	}

	private void setChemin (Point pDepart, Point pArrive) {
		
		//On prend iccarte et on définit la trajectoire
		//On part du point d'arrivé et on va vers l'arrivé en prenant tjrs le point le plus petit
		chemin.push(pArrive);
		Point pPoint=new Point();
		
		//tant que le point de départ n'est pas ajouté à la pile on continue
		
		while ((chemin.peek().getY()!=pDepart.getY())||(chemin.peek().getX()!=pDepart.getX())){
			
			//Récupération du point le plus récent
			pPoint=chemin.peek();
			
			//Création d'un point buffer que l'on sait erroné
			Point pPointBuff=new Point(-1,-1);
			
			//SI le point suivant est en dehors de la matrice on ne lle fait pas.
			if (!((pPoint.getPointBas().getX()<0) || (pPoint.getPointBas().getY()<0) || (pPoint.getPointBas().getY()>= iCCarte[0].length) || (pPoint.getPointBas().getX() >= iCCarte.length))) {

				//Si le point posséde une valeur de totale (On a donc passé par ce point lors de la recherche de point)
				if (iCCarte[pPoint.getPointBas().getX()][pPoint.getPointBas().getY()].getCost_total()!=0){
					
					//Si le cout pour arrivé du point bas est supérieur au point actuelle
					if (iCCarte[pPoint.getPointBas().getX()][pPoint.getPointBas().getY()].getCost_to_D()<iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D()){
					
						//On met le point bas en tant que point buffeur
						pPointBuff=pPoint.getPointBas();
					
					}
					
				}

			}
			if (!((pPoint.getPointHaut().getX()<0) || (pPoint.getPointHaut().getY()<0) || (pPoint.getPointHaut().getY()>= iCCarte[0].length) || (pPoint.getPointHaut().getX() >= iCCarte.length))) {
			
				if (iCCarte[pPoint.getPointHaut().getX()][pPoint.getPointHaut().getY()].getCost_total()!=0){
					
					if (iCCarte[pPoint.getPointHaut().getX()][pPoint.getPointHaut().getY()].getCost_to_D()<iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D()){
											
						//Si le pointbuff est déjà assigné 
						if (pPointBuff.getX()==-1){
							//
							pPointBuff=pPoint.getPointHaut();
						}else {
							//
							if (iCCarte[pPoint.getPointHaut().getX()][pPoint.getPointHaut().getY()].getCost_total()<iCCarte[pPointBuff.getX()][pPointBuff.getY()].getCost_total()){
								pPointBuff=pPoint.getPointHaut();
							}
							
						}
					}
			
				}
			}
			
			if (!((pPoint.getPointDroite().getX()<0) || (pPoint.getPointDroite().getY()<0) || (pPoint.getPointDroite().getY()>= iCCarte[0].length) || (pPoint.getPointDroite().getX() >= iCCarte.length))) {

				if (iCCarte[pPoint.getPointDroite().getX()][pPoint.getPointDroite().getY()].getCost_total()!=0){
					
					if (iCCarte[pPoint.getPointDroite().getX()][pPoint.getPointDroite().getY()].getCost_to_D()<iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D()){
					
						//Si le pointbuff est déjà assigné 
						if (pPointBuff.getX()==-1){
							//
							pPointBuff=pPoint.getPointDroite();
						}else {
							//
							if (iCCarte[pPoint.getPointDroite().getX()][pPoint.getPointDroite().getY()].getCost_total()<iCCarte[pPointBuff.getX()][pPointBuff.getY()].getCost_total()){
								pPointBuff=pPoint.getPointDroite();
							}
							
						}
					}
					
				}
			}
			
			if (!((pPoint.getPointGauche().getX()<0) || (pPoint.getPointGauche().getY()<0) || (pPoint.getPointGauche().getY()>= iCCarte[0].length) || (pPoint.getPointGauche().getX() >= iCCarte.length))) {

				if (iCCarte[pPoint.getPointGauche().getX()][pPoint.getPointGauche().getY()].getCost_total()!=0){
					
					if (iCCarte[pPoint.getPointGauche().getX()][pPoint.getPointGauche().getY()].getCost_to_D()<iCCarte[pPoint.getX()][pPoint.getY()].getCost_to_D()){
					
						//Si le pointbuff est déjà assigné 
						if (pPointBuff.getX()==-1){
							//
							pPointBuff=pPoint.getPointGauche();
						}else {
							//
							if (iCCarte[pPoint.getPointGauche().getX()][pPoint.getPointGauche().getY()].getCost_total()<iCCarte[pPointBuff.getX()][pPointBuff.getY()].getCost_total()){
								pPointBuff=pPoint.getPointGauche();
							}
							
						}
					}
					
				}
			}
		

			if ((pPointBuff.getX()>=0) ||(pPointBuff.getY()>=0)){
			
				//On met le point trouvé dans la stack du chemin.
				chemin.push(pPointBuff);
			
			}else{
				chemin.push(pDepart);
			}
					
			
			
		}
	}
	
}
