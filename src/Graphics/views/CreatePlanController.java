package Graphics.views;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
//import javafx.scene.control.Labeled;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Database.Sql;
import Graphics.PathFinder;
import Path.Point;

public class CreatePlanController {
	
	@FXML
	private Slider width;
	
	@FXML
	private Slider height;
	
	@FXML
	private GridPane plan;
	
	@FXML
	private Button addObstacle;
	
	@FXML
	private Button removeObstacle;
	
	@FXML
	private Button removeAll;
	
	@FXML
	private Button saveButton;
	
	@FXML 
	private Button returnMenu;
	
	@FXML
	private Label widthDisplay;
	
	@FXML
	private Label heightDisplay;
	
	@FXML
	private Label nbObstacles;
	
	@FXML
	private DialogPane popup;
	
	@FXML
	private TextField nomPlan;
	
	@FXML 
	private Button closePop;
	
	@FXML 
	private Button confirmButton;
	
	
	
	private int widthRange;
	private int heightRange;
	private boolean addObstacles;
	private boolean removeObstacles;
	private List<Point> listObstacles;
	
	
	public CreatePlanController(){
		this.removeObstacles = false;
		this.addObstacles = false;
		this.widthRange = 2;
		this.heightRange = 2;
		this.listObstacles = new ArrayList<Point>();
	}
	
	
	@FXML
	public void changeSize(){
		int i;
		int j;
		this.listObstacles.clear();
        nbObstacles.setText(Integer.toString(listObstacles.size()));
		this.widthRange = (int)width.getValue();
		this.heightRange = (int)height.getValue();
		widthDisplay.setText(Integer.toString(widthRange));
		heightDisplay.setText(Integer.toString(heightRange));
		Node tmpNode;
		plan.getChildren().clear();
		plan.getColumnConstraints().clear();
		plan.getRowConstraints().clear();
		for(i=0;i < this.widthRange;i++){
			ColumnConstraints constraint = new ColumnConstraints();
			constraint.setPrefWidth(100);
			constraint.setHgrow(Priority.SOMETIMES);
			plan.getColumnConstraints().add(constraint);
			for(j=0;j < this.heightRange; j++){
				if(i==0){
					RowConstraints rconstraint = new RowConstraints();
					rconstraint.setPrefHeight(100);
					rconstraint.setVgrow(Priority.SOMETIMES);
					plan.getRowConstraints().add(rconstraint);
				}
				tmpNode = new Button();
				tmpNode.setLayoutX(10.0);
				tmpNode.setLayoutY(10.0);
				//((Labeled) tmpNode).setMnemonicParsing(false);
				((Region) tmpNode).setPrefWidth(359.0);
				((Region) tmpNode).setPrefHeight(399.0);
				((ButtonBase) tmpNode).setOnAction((ActionEvent event) -> {
					clickPlan(event);
				});
				plan.add(tmpNode,i,j,1,1);
			}
		}
	}
	
	
	public void addObstacles(){
		this.addObstacles = true;
		this.removeObstacles = false;
	}

	public void removeObstacles(){
		this.removeObstacles = true;
		this.addObstacles = false;
	}
	
	
	
	public void clickPlan(ActionEvent event){
		Integer x;
		Integer y;
		int i;
		Node node;
		Point tmp;
		boolean canAdd;
		node = (Node) event.getSource();
		x = (Integer) GridPane.getColumnIndex(node);
		y = (Integer) GridPane.getRowIndex(node);
	    for (Node nodei : plan.getChildren()) {
	        if (GridPane.getColumnIndex(nodei) == x && GridPane.getRowIndex(nodei) == y) {
	        	tmp = new Point(x,y);
	    		if(addObstacles){
	    	    	canAdd = true;
	    			for(Point iterator : listObstacles){
	    				if((tmp.getX()==iterator.getX())&&(tmp.getY()==iterator.getY())){
	    					canAdd=false;
	    				}
	    			}
	    			if(canAdd){
    					nodei.setStyle("-fx-background-color: black;");
    					listObstacles.add(tmp);
    					nbObstacles.setText(Integer.toString(listObstacles.size()));
	    			}
	    		}else if(removeObstacles) {
		            nodei.setStyle("null");
		            i=0;
		            for(Point iterator: listObstacles){
		            	if((iterator.getX()==x)&&(iterator.getY()==y)){
		            		listObstacles.remove(i);
		            		break;
		            	}
		            	i++;
		            }
		            nbObstacles.setText(Integer.toString(listObstacles.size()));
	    		}
	        }	
	    }
	}
	
	public void removeAll(){
	    for (Node nodei : plan.getChildren()) {
	    	nodei.setStyle("null");
	    	listObstacles.clear();
	    	nbObstacles.setText(Integer.toString(0));
        }	
	}
	
	public void pop(){
		if((popup.isVisible())&&!(popup.isDisable())){
			popup.setVisible(false);
			popup.setDisable(true);
		} else {
			popup.setVisible(true);
			popup.setDisable(false);
		}
	}
	
	public void savePlan(){
		String name;
		name = nomPlan.getText();
		Sql.firstTime();
		Sql.ajoutCarte(this.widthRange, this.heightRange,name, this.listObstacles);
		nomPlan.clear();
	}
	
	public void returnMenu(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/FirstMenu.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Création de plan");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

