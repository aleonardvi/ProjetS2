package Graphics.views;

import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import com.jfoenix.controls.JFXComboBox;
import Database.Sql;
import Graphics.PathFinder;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ModifyPlanController {
	
	@FXML
	private Button modify;
	
	@FXML
	private Button returnButton;
	
	@FXML
	private Button delete;
	
	@FXML 
	private JFXComboBox<String> planChoice;
	
	
	private boolean planChoosed;
	
	
	
	
	
	@PostConstruct
	public void initialize(){
		try{
		planChoice.getItems().clear();
 		ArrayList<String> res = new ArrayList<String>();
		for(ArrayList<String> iterator: Sql.infoPlan()){
			String tmp;
			tmp = (iterator.get(0))+", Largeur :"+iterator.get(1)+", Hauteur :"+iterator.get(2)+", Nombre d'obstacles :"+iterator.get(3);
			res.add(tmp);
		}
		planChoice.getItems().setAll(res);
		} catch (Exception e){
			System.out.println(e);
		}
	}

	
	public void modify(ActionEvent event){
		String tmp;
		planChoosed = (!planChoice.getValue().isEmpty());
		if(this.planChoosed){
			tmp = planChoice.getValue();
			ModifyController.setName(tmp.substring(0, tmp.indexOf(',')));
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			stage.close();
			Stage primaryStage = new Stage();
			AnchorPane root;
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PathFinder.class.getResource("views/Modify.fxml"));
			try {
				root =  (AnchorPane)loader.load();
		        Scene scene = new Scene(root, 1000, 800);
		        primaryStage.setTitle("Modification du plan");
		        primaryStage.setResizable(false);
		        primaryStage.sizeToScene();
				primaryStage.setScene(scene);
				primaryStage.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void delete() {
		String tmp;
		planChoosed = (!planChoice.getValue().isEmpty());
		if(this.planChoosed) {
			tmp = planChoice.getValue();
			Sql.supprimePlan(tmp.substring(0, tmp.indexOf(',')));
			this.initialize();
		}
	}
	
	
	
	
	
	public void returnButton(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/FirstMenu.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Menu");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void returnChoosePlan(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/ChoosePlan.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Sélection du plan");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
}
