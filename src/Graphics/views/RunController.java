package Graphics.views;

import java.io.IOException;
import java.util.List;
import java.util.Stack;

import javax.annotation.PostConstruct;
import Database.Sql;
import Graphics.PathFinder;
import Path.PathFind;
import Path.PathFind_A;
import Path.PathFind_B;
import Path.Point;
import Simulation.Simulation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

public class RunController {
	
	@FXML
	private GridPane carte;
	
	@FXML
	private Button departButton;
	
	@FXML 
	private Button arrivalButton;
	
	@FXML
	private Button returnChoosePlan;
	
	@FXML
	private Button handMade;
	
	@FXML
	private Button returnButton;
	
	@FXML
	private Button beta;
	
	@FXML
	private Button alpha;
	
	@FXML
	private Button savePButton;
	
	@FXML
	private Button run;
	
	@FXML
	private Label duration;
	
	@FXML
	private Label distanceD;

	@FXML
	private Label distanceR;
	
	
	private static String nomPlan;
	private int[][] plan;
	private boolean depart;
	private boolean arrivee;
	private boolean handmade;
	private boolean runned;
	private boolean alphab;
	private boolean betab;
	private Point departPoint;
	private Point arrivalPoint;
	private PathFind result;
	Simulation sim;
	
	@PostConstruct
	public void initialize(){
		int i;
		int j;
		int width;
		int height;
		plan = Sql.chargePlan(nomPlan);
		width = plan.length;
		height = plan[0].length;
		Node tmpNode;
		carte.getChildren().clear();
		carte.getColumnConstraints().clear();
		carte.getRowConstraints().clear();
		carte.setHgap(2.5);
		carte.setVgap(2.5);
		this.depart = false;
		this.arrivee = false;
		this.handmade=false;
		this.runned =false;
		this.alphab = false;
		this.betab=false;
		this.arrivalPoint = null;
		this.departPoint=null;
		this.result = null;
		this.sim = new Simulation();
        duration.setText(Long.toString((long) 0));
        distanceD.setText(Integer.toString(0));
        distanceR.setText(Integer.toString(0));
		savePButton.setVisible(false);
		savePButton.setDisable(true);
		for(i=0;i < width;i++){
			ColumnConstraints constraint = new ColumnConstraints();
			constraint.setPrefWidth(100);
			constraint.setHgrow(Priority.SOMETIMES);
			carte.getColumnConstraints().add(constraint);
			for(j=0;j < height; j++){
				if(i==0){
					RowConstraints rconstraint = new RowConstraints();
					rconstraint.setPrefHeight(100);
					rconstraint.setVgrow(Priority.SOMETIMES);
					carte.getRowConstraints().add(rconstraint);
				}
				tmpNode = new Pane();
				tmpNode.setLayoutX(10.0);
				tmpNode.setLayoutY(10.0);
				tmpNode.setOnMouseClicked((MouseEvent event) -> {
					placePoint(event);					
				});
				switch (plan[i][j]) {
				case 0: tmpNode.setStyle("-fx-background-color: white;");	
					break;
				case 1 : tmpNode.setStyle("-fx-background-color: black;");	
				default:break;
				}
				((Region) tmpNode).setPrefWidth(359.0);
				((Region) tmpNode).setPrefHeight(399.0);
				carte.add(tmpNode,i,j,1,1);
			}
		}
		carte.setGridLinesVisible(true);
	}
	
	public void placeDepart(){
		int i;
		if(runned){
			initialize();
		}
		if(departPoint == null){
			this.depart = true;
		} else {
			i =0;
			List<Node> children = (List<Node>) carte.getChildren();
            for(Node iterator: children){
            	if((GridPane.getColumnIndex(iterator)==departPoint.getX())&&(GridPane.getRowIndex(iterator)==departPoint.getY())){
            		children.get(i).setStyle("-fx-background-color: white;");
            		this.plan[departPoint.getX()][departPoint.getY()] = 0;		
            		departPoint=null;
            		this.depart = true;
            		
            		break;
            	}
            	i++;
            }
		}
	}
	
	public void placeArrivee(){
		int i;
		if(runned){
			initialize();
		}
		if(arrivalPoint == null){
			this.arrivee = true;
		} else {
			i =0;
			List<Node> children = (List<Node>) carte.getChildren();
            for(Node iterator: children){
            	if((GridPane.getColumnIndex(iterator)==arrivalPoint.getX())&&(GridPane.getRowIndex(iterator)==arrivalPoint.getY())){
            		children.get(i).setStyle("-fx-background-color: white;");
            		this.plan[arrivalPoint.getX()][arrivalPoint.getY()]=0;	
            		this.arrivalPoint=null;
            		this.arrivee = true;
            		break;
            	}
            	i++;
            }
		}
	}
	
	public void placePoint(MouseEvent event){
		Node tmp;
		int x;
		int y;
		tmp =(Node) event.getSource();
		x = GridPane.getColumnIndex(tmp);
		y = GridPane.getRowIndex(tmp);
		if ((depart)&&(plan[x][y]==0)) {
			tmp.setStyle("-fx-background-color: #3498db;");
			departPoint = new Point(x,y);
			plan[x][y]=2;
			depart =false;
		}
		if ((arrivee)&&(plan[x][y]==0)) {
			tmp.setStyle("-fx-background-color: #9b59b6;");
			arrivalPoint = new Point(x,y);
			plan[x][y]=3;
			arrivee =false;
			
		}
	}
	
	public void alpha() {
		this.alphab = true;
		this.betab =false;
	}
	
	public void beta() {
		this.alphab = false;
		this.betab =true;
	}
	
	public void handmade(){
		if((departPoint !=null)&&(arrivalPoint!=null)){
			handmade = !handmade;
			if(betab) {
				result = new PathFind_A(0, 0, plan);
				result.chemin.clear();
				betab = false;
			} else if(alphab) {
				result = new PathFind_B(0, 0, plan);
				result.chemin.clear();
				alphab=false;
			}
			for(Node tmp: carte.getChildren()){
				if(handmade){
					this.result = new PathFind_A(plan.length,plan[0].length,plan);
					result.chemin = new Stack<Point>();	
					tmp.setOnMouseClicked((MouseEvent event) ->{
						handmadeWay(event);
					});
				} else {
					tmp.setOnMouseClicked(null);
				}
			}
		}
	}
	
	public void handmadeWay(MouseEvent event){
		Node node;
		boolean canAdd;
		if(handmade){
			node = (Node) event.getSource();
	    	canAdd = true;
			for(Point iterator : result.chemin){
				if((GridPane.getColumnIndex(node)==iterator.getX())&&(GridPane.getRowIndex(node)==iterator.getY())){
					canAdd=false;
				}
			}
			if((canAdd)&&(plan[GridPane.getColumnIndex(node)][GridPane.getRowIndex(node)]==0)) {
					node.setStyle("-fx-background-color:  #bdc3cc");
					Point tmp = new Point(GridPane.getColumnIndex(node),GridPane.getRowIndex(node));
					result.chemin.push(tmp);
			}	
		}
	}
	
	public void run() {
			int i;
			int length;
			length =0;
			this.sim = new Simulation();
			if((departPoint!=null)&&(arrivalPoint!=null)){
				if(!handmade) {
					if(betab) {
						this.result = new PathFind_A(plan.length,plan[0].length,plan);
						result.m_PathFind_A(departPoint, arrivalPoint);	
						result.chemin.pop();
					} else if(alphab) {
						this.result = new PathFind_B(plan.length,plan[0].length,plan);
						result.m_PathFind_A(departPoint, arrivalPoint);	
						result.chemin.pop();
					}
					length = result.chemin.size();
				} else {
					length = result.chemin.size()+1;
				}
				while(!result.chemin.isEmpty()){
					i=0;
					Point iterator = result.chemin.pop();
					List<Node> children = carte.getChildren();
		            for(Node iteratorbis: children){
		            	if((GridPane.getColumnIndex(iteratorbis)==iterator.getX())&&(GridPane.getRowIndex(iteratorbis)==iterator.getY())){
		            		if(!result.chemin.isEmpty()){
		            			if(alphab) {
		            				children.get(i).setStyle("-fx-background-color:  #FF6F7D;");
		            			} else if(betab) {
		            				children.get(i).setStyle("-fx-background-color:  #FEE347;");
		            			} else if(handmade) {
		            				children.get(i).setStyle("-fx-background-color:  #FFDAB9;");
		            			}
		        	            sim.incrementDistance();
		        	            duration.setText(Float.toString(sim.getTime()));
		        	            distanceD.setText(Integer.toString(sim.getDistance()+1));
		        	            distanceR.setText(Integer.toString(length - sim.getDistance()-1));
		            			break;
		            		} else if(handmade) {
	            				children.get(i).setStyle("-fx-background-color:  #FFDAB9;");
		        	            sim.incrementDistance();
		        	            duration.setText(Float.toString(sim.getTime()));
		        	            distanceD.setText(Integer.toString(sim.getDistance()));
		        	            distanceR.setText(Integer.toString(length - sim.getDistance()));
		            			break;
		            		}
		            	}
		            	i++;
		            }
		        }
				sim.incrementDistance();
			}
		this.runned=true;
		savePButton.setVisible(true);
		savePButton.setDisable(false);
        duration.setText(Float.toString(sim.getTime()));
        distanceD.setText(Integer.toString(sim.getDistance()));
        distanceR.setText(Integer.toString(length - sim.getDistance()));
	}
	
	
	public void savePerf() {
		String tmp;
		tmp = new String();
		if(alphab) {
			tmp="alpha";
		} else if(betab) {
			tmp="beta";
		} else if(handmade) {
			tmp="handmade";
		}
		Sql.ajoutParcours(nomPlan, sim.getDistance(), Float.parseFloat(duration.getText()), departPoint, arrivalPoint, tmp);
		initialize();
	}
	
	public void returnButton(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/FirstMenu.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Menu");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void returnChoosePlan(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/ChoosePlan.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Sélection du plan");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void setNomPlan(String tmp){
		nomPlan = tmp;
	}
	
	public String getNomPlan(){
		return RunController.nomPlan;
	}
	
}
