package Graphics.views;

import java.io.IOException;

import Graphics.PathFinder;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.scene.Node;

public class FirstMenuController {
	
	@FXML
	private Button runSimulation;
	
	@FXML 
	private Button createPlan;
	
	@FXML 
	private Button modifyPlan;
	
	
	public void runSimulation(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/ChoosePlan.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Sélection du plan");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void createPlan(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/CreatePlan.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Création de plan");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void modifyPlan(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/ModifyPlan.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Choix de plan à modifier ou supprimer");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

}
