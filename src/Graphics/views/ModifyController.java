package Graphics.views;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import Database.Sql;
import Graphics.PathFinder;
import Path.Point;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

public class ModifyController {

	@FXML
	private Slider width;
	
	@FXML
	private Slider height;
	
	@FXML
	private GridPane plan;
	
	@FXML
	private Button addObstacle;
	
	@FXML
	private Button removeObstacle;
	
	@FXML
	private Button removeAll;
	
	@FXML
	private Button saveButton;
	
	@FXML 
	private Button returnMenu;
	
	@FXML
	private Label widthDisplay;
	
	@FXML
	private Label heightDisplay;
	
	@FXML
	private Label nbObstacles;
	
	@FXML
	private DialogPane popup;
	
	@FXML
	private TextField nomPlan;
	
	@FXML 
	private Button closePop;
	
	@FXML 
	private Button confirmButton;
	
	private int widthV;
	private int heightV;
	private boolean addObstacles;
	private boolean removeObstacles;
	private List<Point> listObstacles;
	private static String namePlan;
	private int[][] carte;
	
	
	@PostConstruct
	public void initialize() {
		int i;
		int j;
		int tmp;
		tmp =0;
		carte = Sql.chargePlan(namePlan);
		widthV = carte.length;
		heightV = carte[0].length;
		Node tmpNode;
		plan.getChildren().clear();
		plan.getColumnConstraints().clear();
		plan.getRowConstraints().clear();
		this.addObstacles = false;
		this.removeObstacles = false;
		this.listObstacles = new ArrayList<Point>();
		widthDisplay.setText(Integer.toString(widthV));
		heightDisplay.setText(Integer.toString(widthV));
		width.setValue(widthV);
		height.setValue(heightV);
		for (i = 0; i < widthV; i++) {
			for (j = 0; j < heightV; j++) {
				if(carte[i][j]==1) {
					tmp++;
					this.listObstacles.add(new Point(i,j));
				}
			}
		}
		nbObstacles.setText(Integer.toString(tmp));
		for(i=0;i < widthV;i++){
			ColumnConstraints constraint = new ColumnConstraints();
			constraint.setPrefWidth(100);
			constraint.setHgrow(Priority.SOMETIMES);
			plan.getColumnConstraints().add(constraint);
			for(j=0;j < heightV; j++){
				if(j==0){
					RowConstraints rconstraint = new RowConstraints();
					rconstraint.setPrefHeight(100);
					rconstraint.setVgrow(Priority.SOMETIMES);
					plan.getRowConstraints().add(rconstraint);
				}
				tmpNode = new Button();
				tmpNode.setLayoutX(10.0);
				tmpNode.setLayoutY(10.0);
				((ButtonBase) tmpNode).setOnAction((ActionEvent event) -> {
					clickPlan(event);					
				});
				if (carte[i][j]==1) {
					tmpNode.setStyle("-fx-background-color: black;");	
				}
				((Region) tmpNode).setPrefWidth(359.0);
				((Region) tmpNode).setPrefHeight(399.0);
				plan.add(tmpNode,i,j,1,1);
			}
		}
	}
	
	@FXML
	public void changeSize(){
		int i;
		int j;
		this.listObstacles.clear();
        nbObstacles.setText(Integer.toString(listObstacles.size()));
		this.widthV = (int)width.getValue();
		this.heightV = (int)height.getValue();
		widthDisplay.setText(Integer.toString(widthV));
		heightDisplay.setText(Integer.toString(heightV));
		Node tmpNode;
		plan.getChildren().clear();
		plan.getColumnConstraints().clear();
		plan.getRowConstraints().clear();
		for(i=0;i < this.widthV;i++){
			ColumnConstraints constraint = new ColumnConstraints();
			constraint.setPrefWidth(100);
			constraint.setHgrow(Priority.SOMETIMES);
			plan.getColumnConstraints().add(constraint);
			for(j=0;j < this.heightV; j++){
				if(i==0){
					RowConstraints rconstraint = new RowConstraints();
					rconstraint.setPrefHeight(100);
					rconstraint.setVgrow(Priority.SOMETIMES);
					plan.getRowConstraints().add(rconstraint);
				}
				tmpNode = new Button();
				tmpNode.setLayoutX(10.0);
				tmpNode.setLayoutY(10.0);
				//((Labeled) tmpNode).setMnemonicParsing(false);
				((Region) tmpNode).setPrefWidth(359.0);
				((Region) tmpNode).setPrefHeight(399.0);
				((ButtonBase) tmpNode).setOnAction((ActionEvent event) -> {
					clickPlan(event);
				});
				plan.add(tmpNode,i,j,1,1);
			}
		}
	}
	
	public void addObstacles(){
		this.addObstacles = true;
		this.removeObstacles = false;
	}

	public void removeObstacles(){
		this.removeObstacles = true;
		this.addObstacles = false;
	}
	
	
	
	public void clickPlan(ActionEvent event){
		Integer x;
		Integer y;
		int i;
		Node node;
		Point tmp;
		boolean canAdd;
		node = (Node) event.getSource();
		x = (Integer) GridPane.getColumnIndex(node);
		y = (Integer) GridPane.getRowIndex(node);
	    for (Node nodei : plan.getChildren()) {
	        if (GridPane.getColumnIndex(nodei) == x && GridPane.getRowIndex(nodei) == y) {
	        	tmp = new Point(x,y);
	    		if(addObstacles){
	    	    	canAdd = true;
	    			for(Point iterator : listObstacles){
	    				if((tmp.getX()==iterator.getX())&&(tmp.getY()==iterator.getY())){
	    					canAdd=false;
	    				}
	    			}
	    			if(canAdd){
    					nodei.setStyle("-fx-background-color: black;");
    					listObstacles.add(tmp);
    					nbObstacles.setText(Integer.toString(listObstacles.size()));
	    			}
	    		}else if(removeObstacles) {
		            nodei.setStyle("null");
		            i=0;
		            for(Point iterator: listObstacles){
		            	if((iterator.getX()==x)&&(iterator.getY()==y)){
		            		listObstacles.remove(i);
		            		break;
		            	}
		            	i++;
		            }
		            nbObstacles.setText(Integer.toString(listObstacles.size()));
	    		}
	        }	
	    }
	}
	
	public void removeAll(){
	    for (Node nodei : plan.getChildren()) {
	    	nodei.setStyle("null");
	    	listObstacles.clear();
	    	nbObstacles.setText(Integer.toString(0));
        }	
	}
	
	public void pop(){
		if((popup.isVisible())&&!(popup.isDisable())){
			popup.setVisible(false);
			popup.setDisable(true);
		} else {
			popup.setVisible(true);
			popup.setDisable(false);
		}
	}
	
	public void savePlan(){
		String name;
		name = nomPlan.getText();
		Sql.firstTime();
		Sql.supprimePlan(namePlan);
		Sql.ajoutCarte(this.widthV, this.heightV,name, this.listObstacles);
		nomPlan.clear();
	}
	
	public void returnMenu(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/FirstMenu.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Création de plan");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		

	public static void setName(String tmp) {
		namePlan =tmp;
	
	}

}
