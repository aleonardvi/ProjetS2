package Graphics.views;

import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import com.jfoenix.controls.JFXComboBox;
import Database.Sql;
import Graphics.PathFinder;
import Path.Point;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ConsultStatsController {
	
	@FXML
	private JFXComboBox<String> consultComboBox;
	
	@FXML
	private Button consultButton;
	
	@FXML
	private Button returnButton;
	
	@FXML
	private Button returChoosePlan;
	
	@FXML
	private BarChart<String, Number> distance;
	
	@FXML
	private BarChart<String, Number> duration;
	
	
	private boolean wayChoosed;
	private static String nomPlan;
	private ArrayList<ArrayList<String>> infos;
	
	
	
	
	@PostConstruct
	public void initialize() {
		this.wayChoosed = false;
		infos = new ArrayList<ArrayList<String>>();
		infos = Sql.infoPerf(nomPlan);
		ArrayList<String> res = new ArrayList<String>();
		for(ArrayList<String> iterator: infos) {
			String tmp;
			tmp = iterator.get(0) + " ("+iterator.get(1) + ";" + iterator.get(2)+")"+ " (" + iterator.get(3) +";" +iterator.get(4)+")";		
			res.add(tmp);	
		}
		consultComboBox.getItems().setAll(res);
		consultComboBox.getItems().add("Global");
		consultComboBox.setValue("Global");		
		consult();
	}
	
	
	public void consult() {
		float sumHD = 0;
		float sumAD = 0;
		float sumBD = 0;
		float sumHT = 0;
		float sumAT = 0;
		float sumBT = 0;
		int nbO = 0;
		String tmp;
		ArrayList<ArrayList<String>> tmpList = new ArrayList<ArrayList<String>>();
		wayChoosed = true;
		distance.getData().clear();
		duration.getData().clear();
		if(wayChoosed) {
			tmp = consultComboBox.getValue();
			if(tmp=="Global") {
				for(ArrayList<String> iterator: infos) {
					Point depPoint = new Point(Integer.parseInt(iterator.get(1)), Integer.parseInt(iterator.get(2)));
					Point ariPoint = new Point(Integer.parseInt(iterator.get(3)), Integer.parseInt(iterator.get(4)));
					tmpList = Sql.comparePerf(nomPlan, depPoint, ariPoint);
					for(ArrayList<String> iteratorbis: tmpList) {
						switch(iteratorbis.get(2)) {
						case "handmade" : sumHT += Float.parseFloat(iteratorbis.get(0));
						sumHD += Float.parseFloat(iteratorbis.get(1));
						break;
						case "alpha" :sumAT += Float.parseFloat(iteratorbis.get(0));
						sumAD += Float.parseFloat(iteratorbis.get(1));
						break;
						case "beta" :sumBT += Float.parseFloat(iteratorbis.get(0));
						sumBD += Float.parseFloat(iteratorbis.get(1));
						break;	
						}
					}
					nbO++;
				}
				sumHD /= nbO;
				sumAD /= nbO;
				sumBD /= nbO;
				sumHT /= nbO;
				sumAT /= nbO;
				sumBT /= nbO;
			} else {
				tmp = consultComboBox.getValue();
				tmpList = Sql.comparePerf(nomPlan, new Point(Integer.parseInt(tmp.substring(tmp.indexOf('(')+1,tmp.indexOf(';'))),Integer.parseInt(tmp.substring(tmp.indexOf(';')+1,tmp.indexOf(')')))), new Point(Integer.parseInt(tmp.substring(tmp.lastIndexOf('(')+1,tmp.lastIndexOf(';'))),Integer.parseInt(tmp.substring(tmp.lastIndexOf(';')+1,tmp.lastIndexOf(')')))));
				for(ArrayList<String> iteratorbis: tmpList) {
					switch(iteratorbis.get(2)) {
					case "handmade" : sumHT = Float.parseFloat(iteratorbis.get(0));
					sumHD = Float.parseFloat(iteratorbis.get(1));
					break;
					case "alpha" :sumAT = Float.parseFloat(iteratorbis.get(0));
					sumAD = Float.parseFloat(iteratorbis.get(1));
					break;
					case "beta" :sumBT = Float.parseFloat(iteratorbis.get(0));
					sumBD = Float.parseFloat(iteratorbis.get(1));
					break;	
					}
				}
			}
			XYChart.Series<String, Number> homemadeD = new XYChart.Series<String, Number>();
			XYChart.Series<String, Number> alphaD = new XYChart.Series<String, Number>();
			XYChart.Series<String, Number> betaD = new XYChart.Series<String, Number>();
			XYChart.Series<String, Number> homemadeT = new XYChart.Series<String, Number>();
			XYChart.Series<String, Number> alphaT = new XYChart.Series<String, Number>();
			XYChart.Series<String, Number> betaT = new XYChart.Series<String, Number>();
	        homemadeD.getData().add(new XYChart.Data<String, Number>("", sumHD));
	        homemadeT.getData().add(new XYChart.Data<String, Number>("", sumHT));  
	        alphaD.getData().add(new XYChart.Data<String, Number>("", sumAD));
	        alphaT.getData().add(new XYChart.Data<String, Number>("", sumAT));  
	        betaD.getData().add(new XYChart.Data<String, Number>("", sumBD));
	        betaT.getData().add(new Data<String, Number>("", sumBT));  	
	        duration.getData().add(homemadeT);
	        duration.getData().add(alphaT);
	        duration.getData().add(betaT);
	        distance.getData().add(homemadeD);
	        distance.getData().add(alphaD);
	        distance.getData().add(betaD);
	        
		}
	}
	
	
	public void returnButton(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/FirstMenu.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Menu");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void returnChoosePlan(ActionEvent event){
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
		Stage primaryStage = new Stage();
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/ChoosePlan.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Sélection du plan");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void setNomPlan(String tmp) {
		nomPlan = tmp;	
	}
}
