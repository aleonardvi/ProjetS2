package Graphics;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;


public class PathFinder extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		AnchorPane root;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PathFinder.class.getResource("views/FirstMenu.fxml"));
		try {
			root =  (AnchorPane)loader.load();
	        Scene scene = new Scene(root, 1000, 800);
	        primaryStage.setTitle("Menu");
	        primaryStage.setResizable(false);
	        primaryStage.sizeToScene();
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
