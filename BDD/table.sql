/*DROP TABLE IF EXISTS ObstacleDuPlan;
DROP TABLE IF EXISTS Plan;
DROP TABLE IF EXISTS Obstacle;*/

CREATE TABLE Plan (
	   nomP varchar(32),
	   idP int(8) NOT NULL UNIQUE AUTO_INCREMENT
	   );


CREATE TABLE ObstacleDuPlan (
	   idObstacle int(8),
	   posX int(16) NOT NULL,
	   posY int(16) NOT NULL,
	   idPlan int(8),

	   FOREIGN KEY (idObstacle) REFERENCES Obstacle(idOb),
	   FOREIGN KEY (idPlan) REFERENCES Plan(idP)
	   );


CREATE TABLE Parcours (
       tempsP int(16),
       distanceP int(16),
	   idPlan int(8),

	   FOREIGN KEY (idPlan) REFERENCES Plan(idP)
	   );
